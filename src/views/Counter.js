import React from 'react';

function Counter({counter,onIncrement,onDecrement}) {
  return (
    <div>
      <div>
        <button class="btn-counter" onClick={onIncrement}>
          Increment
        </button>

        <button  class="btn-counter" onClick={onDecrement}>
          Decrement
        </button>
      </div>

      <div>
        <p>Nombre de click : {counter}</p>
      </div>
    </div>
  );
};

export default Counter;