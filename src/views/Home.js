import React from 'react';
import Counter from '../containers/CounterContainer';

function Home() {
  return (
    <div>
      <Counter></Counter>
    </div>
  );
};

export default Home;
