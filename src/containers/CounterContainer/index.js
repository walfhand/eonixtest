import React from 'react';
import reducer from './reducer';
import Counter from '../../views/Counter';
import { connect } from "react-redux";
import { makeSelectCounterContainer } from "./selector";
import { createStructuredSelector } from 'reselect';
import { decrementAction, incrementAction } from './action';
import { useInjectReducer } from '../../utils/injectReducer';

const key = 'counterContainer';

function CounterContainer(props){
    useInjectReducer({key,reducer})
    return (<Counter{...props}/>)
  }

const mapStateToProps = createStructuredSelector({
    counter: makeSelectCounterContainer(),
  });

export function mapDispatchToProps(dispatch) {
    return {
      dispatch,
      onIncrement: () => dispatch(incrementAction()),
      onDecrement: () => dispatch(decrementAction())
    };
  }

export default connect(mapStateToProps,mapDispatchToProps)(CounterContainer)